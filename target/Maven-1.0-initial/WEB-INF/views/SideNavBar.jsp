<div class="col-md-2 bootstrap-admin-col-left">
                    
                    <ul class="nav navbar-collapse collapse bootstrap-admin-navbar-side">
                        
                        
                        <li class="active">
                            <a href="Dashboard"><i class="glyphicon glyphicon-home"></i> Dashboard</a>
                        </li>
                    
                    </ul>
                    
                    <br/>
                    
                 
                    
                    <ul class="nav navbar-collapse collapse bootstrap-admin-navbar-side">

                        <li class="active">
                            <a href=""><i class="glyphicon glyphicon-road"></i> Manage Concert </a>
                            <ul class="nav navbar-collapse bootstrap-admin-navbar-side">
                                <li><a href="AddCompetition"><i class="glyphicon glyphicon-chevron-right"></i> Add Concert </a></li>
                                <li><a href="DisplayCompetition"><i class="glyphicon glyphicon-chevron-right"></i> Display Concert </a></li>
                            </ul>
                        </li>

                    </ul>
                    
                    <br/>
                    
                    <ul class="nav navbar-collapse collapse bootstrap-admin-navbar-side">

                        <li class="active">
                            <a href=""><i class="glyphicon glyphicon-globe"></i> Manage Songs </a>
                            <ul class="nav navbar-collapse bootstrap-admin-navbar-side">
                               <li><a href="AddSeminar"><i class="glyphicon glyphicon-chevron-right"></i> Add Song Details </a></li>
                               <li><a href="DisplaySeminar"><i class="glyphicon glyphicon-chevron-right"></i> Display Song Details </a></li>
                            </ul>
                        </li>

                    </ul>
                    
                    <br/>
                    
                   
                    <ul class="nav navbar-collapse collapse bootstrap-admin-navbar-side">

                        <li class="active">
                            <a href=""><i class="glyphicon glyphicon-user"></i> Manage User </a>
                            <ul class="nav navbar-collapse bootstrap-admin-navbar-side">
                                <li><a href="AddUser"><i class="glyphicon glyphicon-chevron-right"></i> Add User </a></li>
                                <li><a href="DisplayUser"><i class="glyphicon glyphicon-chevron-right"></i> Display User </a></li>
                            </ul>
                        </li>

                    </ul>
                    
                    
                </div>