<!-- small navbar -->
    
    <!-- main / large navbar -->
    <nav class="navbar navbar-default navbar-fixed-top bootstrap-admin-navbar " role="navigation">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".main-navbar-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="Dashboard"><img alt="Event Management System" src="assets/adminassets/images/adminlogo.jpg"/></a>
                    </div>
                    
       
                    
                    <div class="collapse navbar-collapse main-navbar-collapse">             
                        
                        <ul class="nav navbar-nav">
                            
                            <li><a href="Dashboard">Home</a></li>
                            
                            <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-hover="dropdown">User<b class="caret"></b></a>
                            <ul class="dropdown-menu">
                               
                                <li><a href="#">Profile</a></li>
                                <li><a href="<c:url value="j_spring_security_logout" />" > Logout</a> </li>
                                
                                
                                
                            </ul>
                            </li>  
                        </ul>
                    </div><!-- /.navbar-collapse -->
                </div>
            </div>
        </div><!-- /.container -->
    </nav>