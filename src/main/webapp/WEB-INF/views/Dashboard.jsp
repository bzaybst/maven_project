
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page session="true"%>
<!DOCTYPE html>
<html>
<head>
    <title>Dashboard | Band Management System</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <!-- Bootstrap -->
        <link rel="stylesheet" media="screen" href="assets/adminassets/css/bootstrap.min.css">
        <link rel="stylesheet" media="screen" href="assets/adminassets/css/bootstrap-theme.min.css">

        <!-- Bootstrap Admin Theme -->
        <link rel="stylesheet" media="screen" href="assets/adminassets/css/bootstrap-admin-theme.css">
        <link rel="stylesheet" media="screen" href="assets/adminassets/css/bootstrap-admin-theme-change-size.css">

        <!-- Vendors -->
        <link rel="stylesheet" media="screen" href="assets/adminassets/vendors/easypiechart/jquery.easy-pie-chart.css">
        <link rel="stylesheet" media="screen" href="assets/adminassets/vendors/easypiechart/jquery.easy-pie-chart_custom.css">

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
           <script type="text/javascript" src="js/html5shiv.js"></script>
           <script type="text/javascript" src="js/respond.min.js"></script>
        <![endif]-->

</head>
   
<body class="bootstrap-admin-with-small-navbar">
        <!-- small navbar -->
        <%@include file="TopNavBar.jsp" %>
        <!-- main / large navbar -->
       

        <div class="container">
            <!-- left, vertical navbar & content -->
             
           <div class="row">
                <!-- left, vertical navbar -->
                <%@include file="SideNavBar.jsp" %>
                
                <!-- content -->
                <div class="col-md-10">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="page-header">
                                <h1>Dashboard</h1>
                                
                            </div>
                        </div>
                    </div>

                 

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default bootstrap-admin-no-table-panel">
                                <div class="panel-heading">
                                    <div class="text-muted bootstrap-admin-box-title">WELCOME TO ADMIN PANEL</div>
                                    
                                </div>
                               
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <!-- footer -->
        <%@include file="Footer.jsp" %>

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script type="text/javascript" src="http://code.jquery.com/jquery-2.0.3.min.js"></script>

        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script type="text/javascript" src="assets/adminassets/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="assets/adminassets/js/twitter-bootstrap-hover-dropdown.min.js"></script>
        <script type="text/javascript" src="assets/adminassets/js/bootstrap-admin-theme-change-size.js"></script>
       

      
        
        
    </body>
    
    
</html>
