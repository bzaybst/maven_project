<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    


<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Dashboard</title>
    <link href="assets/lassets/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/lassets/css/font-awesome.min.css" rel="stylesheet">
	<link href="assets/lassets/css/main.css" rel="stylesheet">
	<link href="assets/lassets/css/animate.css" rel="stylesheet">	
	<link href="assets/lassets/css/responsive.css" rel="stylesheet">

    <!--[if lt IE 9]>
	    <script src="js/html5shiv.js"></script>
	    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="assets/lassets/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/lassets/images/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/lassets/images/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/lassets/images/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="assets/lassets/images/apple-touch-icon-57-precomposed.png">
</head><!--/head-->

<body>
	<header id="header" role="banner">		
		<div class="main-nav">
			<div class="container">
				
		        <div class="row">	        		
		            <div class="navbar-header">
		                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
		                    <span class="sr-only">Toggle navigation</span>
		                    <span class="icon-bar"></span>
		                    <span class="icon-bar"></span>
		                    <span class="icon-bar"></span>
		                </button>
		               
		                	<img class="img-responsive navbar-brand" src="assets/lassets/images/images.jpg" alt="logo">
		                                   
		            </div>
		            <div class="collapse navbar-collapse">
		                <ul class="nav navbar-nav navbar-right">                 
		                    <li class="scroll active"><a href="#home">Home</a></li>
		                    <li class="scroll"><a href="#event">Explore</a></li>                         
		                    <li class="scroll"><a href="#explore">Event</a></li>
		                    <li class="scroll"><a href="#about">About</a></li>                    
		                    <li><a class="no-scroll" href="Dashboard" target="_blank">Login</a></li>
		                    <li class="scroll"><a href="#contact">Contact</a></li>       
		                </ul>
		            </div>
		        </div>
	        </div>
        </div>                    
    </header>
    <!--/#header--> 

    <section id="home">	
		<div id="main-slider" class="carousel slide" data-ride="carousel">
			<ol class="carousel-indicators">
				<li data-target="#main-slider" data-slide-to="0" class="active"></li>
				<li data-target="#main-slider" data-slide-to="1"></li>
				<li data-target="#main-slider" data-slide-to="2"></li>
			</ol>
			<div class="carousel-inner">
				<div class="item active">
					<img class="img-responsive" src="assets/lassets/images/slider/slideimg.jpg" alt="slider">						
					<div class="carousel-caption">
						<h2>OUR MUSIC </h2>
						
						<p>Reflect ART and Culture <i class="fa fa-angle-right"></i></p>
					</div>
				</div>
				<div class="item">
					<img class="img-responsive" src="assets/lassets/images/slider/slideimg2.jpg" alt="slider">	
					<div class="carousel-caption">
						<h2>Music</h2>
						
						<p>Music is the meditation between spiritual and sensual life <i class="fa fa-angle-right"></i></p>
					</div>
				</div>
				<div class="item">
					<img class="img-responsive" src="assets/lassets/images/slider/slide3.jpg" alt="slider">	
					<div class="carousel-caption">
						<h2>FOR EVENTS </h2>
						
						<a href="#contact" >Contact Us<i class="fa fa-angle-right"></i></a>
					</div>
				</div>				
			</div>
		</div>    	
    </section>
	<!--/#home-->

	<section id="explore">
		<div class="container">
			<div class="row">
				<div class="watch">
					
				</div>				
				<div class="col-md-4 col-md-offset-2 col-sm-5">
					<h2>our next event in</h2>
				</div>				
				<div class="col-sm-7 col-md-6">					
					<ul id="countdown">
						<li>					
							<span>26th</span>
							
						</li>
						<li>
							<span>May</span>
							
						</li>
						<li>
							<span>2018</span>
							
						</li>
									
					</ul>
				</div>
			</div>
			
		</div>
	</section><!--/#explore-->

	<section id="event">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-9">
					<div id="event-carousel" class="carousel slide" data-interval="false">
						<h2 class="heading">OUR BAND MEMBER</h2>
						<a class="even-control-left" href="#event-carousel" data-slide="prev"><i class="fa fa-angle-left"></i></a>
						<a class="even-control-right" href="#event-carousel" data-slide="next"><i class="fa fa-angle-right"></i></a>
						<div class="carousel-inner">
							<div class="item active">
								<div class="row">
									<div class="col-sm-4">
										<div class="single-event">
											<img class="img-responsive" src="assets/lassets/images/event/band1.jpg" alt="event-image">
											<h4>Simpsons Morrison</h4>
											<h5>Vocal</h5>
										</div>
									</div>
									<div class="col-sm-4">
										<div class="single-event">
											<img class="img-responsive" src="assets/lassets/images/event/band2.jpg" alt="event-image">
											<h4>Spike</h4>
											<h5>vocals, rhythm guitar</h5>
										</div>
									</div>
									<div class="col-sm-4">
										<div class="single-event">
											<img class="img-responsive" src="assets/lassets/images/event/band3.jpg" alt="event-image">
											<h4>Rob Bourdon</h4>
											<h5>Lead Guitarists</h5>
										</div>
									</div>
								</div>
							</div>
							<div class="item">
								<div class="row">
									<div class="col-sm-4">
										<div class="single-event">
											<img class="img-responsive" src="assets/lassets/images/event/band4.jpg" alt="event-image">
											<h4>Bennington</h4>
											<h5>Drummer</h5>
										</div>
									</div>
									<div class="col-sm-4">
										<div class="single-event">
											<img class="img-responsive" src="assets/lassets/images/event/band5.jpg" alt="event-image">
											<h4>Shinoda</h4>
											<h5>Band manager</h5>
										</div>
									</div>
									<div class="col-sm-4">
										<div class="single-event">
											<img class="img-responsive" src="assets/lassets/images/event/band6.jpg" alt="event-image">
											<h4>Bourdon</h4>
											<h5>Event Manager</h5>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="guitar">
					<img class="img-responsive" src="assets/lassets/images/download.jpg" alt="guitar">
				</div>
			</div>			
		</div>
	</section><!--/#event-->

	<section id="about">
		<div class="guitar2">				
			<img class="img-responsive" src="assets/lassets/images/aboutme2.jpg" alt="guitar">
		</div>
		<div class="about-content">					
					<h2>About OUR BAND</h2>
					<p>We ARE THE ROCK BAND FROM NEPAL. WE HAVE PERFORM IN LOTS OF PLACES. WE ARE VERY MUCH PASSIONATE ABOUT THE MUSIC, CULTURE. WE REVIVE MUSIC WITH AN ART AND CULTURE.</p>
					<a href="#" class="btn btn-primary">View Date & Place <i class="fa fa-angle-right"></i></a>
				
		</div>
	</section><!--/#about-->
	
	
	
	

	<section id="contact">
		
		<div class="contact-section">
			<div class="ear-piece">
				<img class="img-responsive" src="assets/lassets/images/contactimg.jpg" alt="">
			</div>
			<div class="container">
				<div class="row">
					<div class="col-sm-3 col-sm-offset-4">
						<div class="contact-text">
							<h3>Contact</h3>
							<address>
								E-mail: ourband@events.com<br>
								Phone: +977-0000000<br>
								Mobile: +977- 9800000000
							</address>
						</div>
						<div class="contact-address">
							<h3>Contact</h3>
							<address>
								Tradetower,Thapathali<br>
								Kathmandu<br>
								Nepal
							</address>
						</div>
					</div>
					<div class="col-sm-5">
						<div id="contact-section">
							<h3>Send a message</h3>
					    	<div class="status alert alert-success" style="display: none"></div>
					    	<form id="main-contact-form" class="contact-form" name="contact-form" method="post" action="sendemail.php">
					            <div class="form-group">
					                <input type="text" name="name" class="form-control" required="required" placeholder="Name">
					            </div>
					            <div class="form-group">
					                <input type="email" name="email" class="form-control" required="required" placeholder="Email ID">
					            </div>
					            <div class="form-group">
					                <textarea name="message" id="message" required="required" class="form-control" rows="4" placeholder="Enter your message"></textarea>
					            </div>                        
					            <div class="form-group">
					                <button type="submit" class="btn btn-primary pull-right">Send</button>
					            </div>
					        </form>	       
					    </div>
					</div>
				</div>
			</div>
		</div>		
	</section>
    <!--/#contact-->

    <footer id="footer">
        <div class="container">
            <div class="text-center">
                <p> Copyright  &copy;2018<a target="_blank" href="#"> ROCKERs</a>MUSICAL BAND <br> TO PRESERVE ART AND CULTURE</p>                
            </div>
            <div class="footer-top">
					<div class="pull-right social-icons">
						<a href="#"><i class="fa fa-twitter"></i></a>
						<a href="#"><i class="fa fa-facebook"></i></a>
						<a href="#"><i class="fa fa-google-plus"></i></a>
						<a href="#"><i class="fa fa-youtube"></i></a>
					</div>
				</div>     
        </div>
    </footer>
    <!--/#footer-->
  
    <script type="text/javascript" src="assets/lassets/js/jquery.js"></script>
    <script type="text/javascript" src="assets/lassets/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>
  	<script type="text/javascript" src="assets/lassets/js/gmaps.js"></script>
	<script type="text/javascript" src="assets/lassets/js/smoothscroll.js"></script>
    <script type="text/javascript" src="assets/lassets/js/jquery.parallax.js"></script>
    <script type="text/javascript" src="assets/lassets/js/coundown-timer.js"></script>
    <script type="text/javascript" src="assets/lassets/js/jquery.scrollTo.js"></script>
    <script type="text/javascript" src="assets/lassets/js/jquery.nav.js"></script>
    <script type="text/javascript" src="assets/lassets/js/main.js"></script>  
</body>

</html>
