/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itn.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Rebel
 */
@Entity
@Table(name = "competition", catalog = "eventmanagement", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Competition.findAll", query = "SELECT c FROM Competition c"),
    @NamedQuery(name = "Competition.findByCid", query = "SELECT c FROM Competition c WHERE c.cid = :cid"),
    @NamedQuery(name = "Competition.findByCompetitionname", query = "SELECT c FROM Competition c WHERE c.competitionname = :competitionname"),
    @NamedQuery(name = "Competition.findByCompetitiondate", query = "SELECT c FROM Competition c WHERE c.competitiondate = :competitiondate"),
    @NamedQuery(name = "Competition.findByCompetitionduration", query = "SELECT c FROM Competition c WHERE c.competitionduration = :competitionduration"),
    @NamedQuery(name = "Competition.findByCompetitionbuilding", query = "SELECT c FROM Competition c WHERE c.competitionbuilding = :competitionbuilding"),
    @NamedQuery(name = "Competition.findByCompetitionmaster", query = "SELECT c FROM Competition c WHERE c.competitionmaster = :competitionmaster")})
public class Competition implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "cid")
    private Integer cid;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 23)
    @Column(name = "competitionname")
    private String competitionname;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 23)
    @Column(name = "competitiondate")
    private String competitiondate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 23)
    @Column(name = "competitionduration")
    private String competitionduration;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 23)
    @Column(name = "competitionbuilding")
    private String competitionbuilding;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 23)
    @Column(name = "competitionmaster")
    private String competitionmaster;

    public Competition() {
    }

    public Competition(Integer cid) {
        this.cid = cid;
    }

    public Competition(Integer cid, String competitionname, String competitiondate, String competitionduration, String competitionbuilding, String competitionmaster) {
        this.cid = cid;
        this.competitionname = competitionname;
        this.competitiondate = competitiondate;
        this.competitionduration = competitionduration;
        this.competitionbuilding = competitionbuilding;
        this.competitionmaster = competitionmaster;
    }

    public Integer getCid() {
        return cid;
    }

    public void setCid(Integer cid) {
        this.cid = cid;
    }

    public String getCompetitionname() {
        return competitionname;
    }

    public void setCompetitionname(String competitionname) {
        this.competitionname = competitionname;
    }

    public String getCompetitiondate() {
        return competitiondate;
    }

    public void setCompetitiondate(String competitiondate) {
        this.competitiondate = competitiondate;
    }

    public String getCompetitionduration() {
        return competitionduration;
    }

    public void setCompetitionduration(String competitionduration) {
        this.competitionduration = competitionduration;
    }

    public String getCompetitionbuilding() {
        return competitionbuilding;
    }

    public void setCompetitionbuilding(String competitionbuilding) {
        this.competitionbuilding = competitionbuilding;
    }

    public String getCompetitionmaster() {
        return competitionmaster;
    }

    public void setCompetitionmaster(String competitionmaster) {
        this.competitionmaster = competitionmaster;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cid != null ? cid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Competition)) {
            return false;
        }
        Competition other = (Competition) object;
        if ((this.cid == null && other.cid != null) || (this.cid != null && !this.cid.equals(other.cid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.itn.entity.Competition[ cid=" + cid + " ]";
    }
    
}
