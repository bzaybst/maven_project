/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itn.dao;


import com.itn.entity.Seminar;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Rebel
 */
@Repository
public class SeminarDao implements SeminarDaoInterface {
@Autowired
    
SessionFactory sessionFactory;

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
    
    @Override
    public void insert(Seminar s) {
       Session sess=sessionFactory.openSession();
        sess.save(s);
        sess.close(); 
    }

    @Override
    public List<Seminar> display() {
        Session sess=sessionFactory.openSession();
    List<Seminar> s=sess.createCriteria(Seminar.class).list();
        return s;
    }

    @Override
    public void delete(int sid) {
        Session sess=sessionFactory.openSession();
        Transaction tx=sess.beginTransaction();
        Seminar s=(Seminar)sess.load(Seminar.class, sid);       
        sess.delete(s);
        tx.commit();
    
    }

    @Override
    public Seminar display_by_id(int sid) {
        Session sess=sessionFactory.openSession();
             Seminar s=(Seminar)sess.get(Seminar.class, sid); 
             return s;
    }

    @Override
    public void update(Seminar s) {
        Session sess=sessionFactory.openSession();
        Transaction tx=sess.beginTransaction();
        sess.update(s);
        tx.commit();
    }
    
}
