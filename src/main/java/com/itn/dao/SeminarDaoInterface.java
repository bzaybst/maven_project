/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itn.dao;

import com.itn.entity.Seminar;
import java.util.List;

/**
 *
 * @author Rebel
 */
public interface SeminarDaoInterface {
    public void insert(Seminar s);
    public List<Seminar>display();
    public void delete(int sid);
    public Seminar display_by_id(int sid);
    public void update(Seminar s); 
}
