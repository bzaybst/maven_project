/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itn.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author Rebel
 */
@Controller
public class CourseController {
    
    @RequestMapping(value="/AddCourse",method=RequestMethod.GET)
    
    public ModelAndView addcourse(){
        return new ModelAndView("AddCourse");
    }   
     @RequestMapping(value="/DisplayCourse",method=RequestMethod.GET)
    
    public ModelAndView displaycourse(){
        return new ModelAndView("DisplayCourse");
    }   
    @RequestMapping(value="/Dashboard",method=RequestMethod.GET)
    public ModelAndView dashboard(){
        return new ModelAndView("Dashboard");
    }
    
}
